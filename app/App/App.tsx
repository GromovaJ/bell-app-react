import 'bootstrap/dist/css/bootstrap.min.css';
import * as React from 'react';
import {connect} from 'react-redux';
import {Dispatch} from 'redux';
import {isCorrectLogin} from '../common/isCorrectLogin';
import {IStoreState} from '../reducers';
import {Actions, IDispatchProps} from '../Actions/Actions';

interface IStateProps {
  user: string,
  password: string,
  loginStatus: boolean;
  loading: boolean;
}
interface IState {
  user: string;
  password: string;
  loginStatus: boolean;
  loading: boolean;
}
interface IStatePr {
  history: any;
}
type TProps = IDispatchProps & IStateProps & IStatePr;

class App extends React.Component<TProps, IState> {
  constructor(props: any) {
        super(props);
        this.state = {user: props.user || '',
                      password: props.password || '',
                      loginStatus: props.loginStatus || false,
                      loading: props.loading || false
                    }
      }

  // handleClick = () => {
  //   console.log('handleClick', this.props);
  //   const {actions} = this.props;
  //   actions.onClick({user: this.userInput.value, password: this.passwordInput.value});
  // };

  handleLogin = () => {
    const {actions} = this.props;
    console.log('%%%%%handleLogin-props',this.props);
    actions.onLogin(this.props.user, this.props.password);
    if (isCorrectLogin(this.props.user, this.props.password)) {
      this.props.history.push('/org');
    }
  };

  handleLogout = () => {
    const {actions} = this.props;
    actions.onLogout();
  };

  handleChangeUser = (e: any) => {
   //const value = e.currentTarget.value
   //const fieldName = e.currentTarget.dataset.fieldName
   this.props.actions.onLoginUser(e.currentTarget.value)
   // this.setState(prev => ({
   //   ...prev,
   //   [fieldName]: value,
   // }));
 }

 handleChangePassword = (e: any) => {
  //const value = e.currentTarget.value
  //const fieldName = e.currentTarget.dataset.fieldName
  this.props.actions.onLoginPassword(e.currentTarget.value);
}

  render () {
    console.log('PROPS',this.props);
    console.log('STATE',this.state);
    const {loginStatus, loading} = this.props;
    return (
      <div>

      <div className="container">
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <h4 className="modal-title" id="myModalLabel">Регистрация</h4>
              <div>
                {
                  loading ?
                    <p>Авторизация...</p> :
                    loginStatus ?
                      <p>
                        Login success
                      </p> :
                      <p>
                        Logged out
                      </p>
                }
              </div>
            </div>
            <div className="modal-body">
              <form id="myForm">
                <div className="form-group has-feedback">
                  <label className="control-label">Введите логин (admin):</label>
                  <div className="input-group">
                    <span className="input-group-addon"><i className="glyphicon glyphicon-user"></i></span>
                    <input
                      className="form-control"
                      data-field-name={'user'}
                      type={'text'}
                      disabled={loginStatus}
                    //  value={this.state.user||''}
                    //  onChange={this.handleChange}
                      onChange={this.handleChangeUser}
                    />
                  </div>
                </div>
                <div className="form-group has-feedback">
                  <label className="control-label">Введите пароль (12345):</label>
                  <div className="input-group">
                    <span className="input-group-addon"><i className="glyphicon glyphicon-envelope"></i></span>
                    <input
                      className="form-control"
                      data-field-name={'password'}
                      type={'password'}
                      disabled={loginStatus}
                      //value={this.state.password}
                      onChange={this.handleChangePassword}
                    />
                  </div>
                </div>
              </form>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-default"
                data-dismiss="modal"
                disabled={loading}
                onClick={this.handleLogout}
                >Отмена
              </button>
              <button
                id="save"
                type="button"
                className="btn btn-primary"
                disabled={loginStatus}
                onClick={this.handleLogin}
                >Регистрация
              </button>

            </div>
          </div>
        </div>
      </div>

      </div>
    );
  }
};

function mapStateToProps(state: IStoreState, ownProps: any): IStateProps {
  console.log('ownstate',state);
  console.log('ownProps',ownProps);
  return {
    user: state.login.user,
    password: state.login.password,
    loginStatus: state.login.loginStatus,
    loading: state.login.loading,
  };
}

function mapDispatchToProps(dispatch: Dispatch<IDispatchProps>): IDispatchProps {
  return {
    actions: new Actions(dispatch)
  };
}
const connectApp = connect(mapStateToProps, mapDispatchToProps)(App);

export {connectApp as App};
