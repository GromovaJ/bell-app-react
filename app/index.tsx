import 'bootstrap/dist/css/bootstrap.min.css';
import { ConnectedRouter, connectRouter, routerMiddleware } from 'connected-react-router';
import {createBrowserHistory} from 'history';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import { Route, Switch } from 'react-router';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import {Organization} from './components/Organization';
import {Department} from './components/Department';
import {reducer} from './reducers';
import {App} from './App/App';

const history = createBrowserHistory();

const composeEnhancers = composeWithDevTools({
      // Specify name here, actionsBlacklist, actionsCreators and other options if needed
    });
const store = createStore(
  connectRouter(history)(reducer),
  composeEnhancers(
    applyMiddleware(
      routerMiddleware(history),
    ),
  ),
)
ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <div>
        <Switch>
          <Route exact path="/" component={ App } />
          <Route path="/org" component={ Organization } />
          <Route path="/dept/:id_org" component={ Department }/>
        </Switch>
      </div>
    </ConnectedRouter>
  </Provider>,
  document.getElementById('app')
)
