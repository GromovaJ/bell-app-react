import * as React from 'react';
//import { connect } from 'react-redux';

interface IStateProps {
  onClose: any;
}
interface IState {
  onClose: any;
}

class Modal extends React.Component<IStateProps, IState> {
  render() {
    console.log('modal',this.props);
    // Render nothing if the "show" prop is false
    // if(!this.props.show) {
    //   return null;
    // }

    return (
      <div>
        <div className="modal-body">
          <h1>ModalWindow</h1>
          Редактирование в модальном окне пока работает.
          Редактировать запись можно двойным щелчком мыши по ячейки таблицы.
        </div>
        <div className="modal-footer">
          <button className="btn tnn-default" onClick={this.props.onClose}>Закрыть</button>
          <button className="btn tnn-success">Сохранить</button>
        </div>
      </div>
    );
  }
}

// function mapStateToProps(state){
//   return {
//     modal: state.modal
//   };
// }
// export default connect(mapStateToProps)(Modal);
export default Modal;
//{this.props.children}
