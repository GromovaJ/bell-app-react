import {Dispatch} from 'redux';
import {OrgActionTypes} from './Consts';

export interface IDispatchProps {
  actions: Actions;
}

export class Actions {
  constructor(private dispatch: Dispatch<IDispatchProps>) {
  }

  onClick = (rowSelected: any) => {
    this.dispatch({type: OrgActionTypes.ORGCLICK, payload: rowSelected,});
  }

  onDoubleClick = (rowSelected: any) => this.dispatch({type: OrgActionTypes.ORGDOUBLECLICK, payload: rowSelected,});

  onLoad = () => {
    //this.dispatch({type: `${OrgActionTypes.LOAD}`});

  //  fetch('http://www.mocky.io/v2/5b5ec2812e00007d0069462d') //200 - true
    fetch('http://www.mocky.io/v2/5b6033252f0000630046174a')
      .then(response => {
        if (response.status === 200) {
          return response.json();
        } else {
          throw 'error';
        }
      })
      .then(data => {
        //формат ответа:
        //{"data": {"authorized": true}}
        //console.log('fetch: data=', data);
        this.dispatch({type: `${OrgActionTypes.ORGLOAD}`, payload: data.organizations});
      })
      .catch(error => {
        this.dispatch({type: `${OrgActionTypes.ORGLOAD}`, payload: error});
      });
};

onDelete = (organizations: any) => {
  //const org = organizations.filter(organization => (!(rowKeys.includes(organization.id))));
    this.dispatch({type: OrgActionTypes.ORGDELETE, payload: organizations});
  };

  onInsert = (rowIns: any, organizations: any) => {  //, organizations: any
    //const newRow = {id: 100, ...rowIns};
  //console.log('onInsert', newRow);
     const org = organizations.concat(rowIns);
     this.dispatch({type: OrgActionTypes.ORGINSERT, payload: org});
  };

  onUpdate = (rowKey: any, organizations: any, cellName: any, cellValue: any) => {
    const org = organizations.map(organization => {
      if (organization.id === rowKey) {
        organization[cellName] = cellValue;
      }
      return organization;
    });
    this.dispatch({type: OrgActionTypes.ORGUPDATE, payload: org});
  };
}
