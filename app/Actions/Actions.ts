import {Dispatch} from 'redux';
import {ActionTypes, AsyncActionTypes} from './Consts';

export interface IDispatchProps {
  actions: Actions;
}

export class Actions {
  constructor(private dispatch: Dispatch<IDispatchProps>) {
  }
  onLoginUser = (userValue: any) => this.dispatch({type: `${ActionTypes.LOGIN_USER}`, payload: userValue,});

  onLoginPassword = (passwordValue: any) => this.dispatch({type: `${ActionTypes.LOGIN_PASSWORD}`, payload: passwordValue,});

  onClick = (usernameInput: any) => this.dispatch({type: ActionTypes.CLICK, payload: usernameInput,});

  onLogin = (user: string, password: string) => {
    this.dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}`});
  //  this.dispatch((dispatch: Dispatch<IDispatchProps>) => {
    //this.dispatch((dispatch: Dispatch<IDispatchProps>) => {
      //Простейший асинхронный экшен
      // setTimeout(() => {
      //   dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`});
      // }, 2000)

       // let url;
       // if (isCorrectLogin(user, password)) {
       //   url = 'http://www.mocky.io/v2/5aafaf6f2d000057006eff31';
       // }
       // else url = 'http://www.mocky.io/v2/5aafafa32d000056006eff3b';

      //url = 'http://www.mocky.io/v2/5aafaf6f2d000057006eff31';
      //Экшен для запроса к РЕСТам
      //fetch('http://www.mocky.io/v2/5aafaf2c2d000048006eff2c') //404
      fetch('http://www.mocky.io/v2/5aafaf6f2d000057006eff31') //200 - true
      //fetch('http://www.mocky.io/v2/5aafafa32d000056006eff3b') //200 - false

      //fetch(url)
        .then(response => {
          if (response.status === 200) {
            return response.json();
          } else {
            throw 'error';
          }
        })
        .then(data => {
          //формат ответа:
          const dataUser = {...data, user, password};
          this.dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`, payload: dataUser});
        })
        .catch(error => {
          this.dispatch({type: `${ActionTypes.LOGIN}${AsyncActionTypes.FAILURE}`, payload: error});
        });
  //  });
  };

  onLogout = () => {
    console.log('onLogout');
    const dataUser = {user: '', password : ''};
    this.dispatch({type: `${ActionTypes.LOGOUT}`, payload: dataUser}); //Изменяем Store -> Props
};
}
