import {Dispatch} from 'redux';
import {DeptActionTypes} from './Consts';

export interface IDispatchProps {
  actions: Actions;
}

export class Actions {
  constructor(private dispatch: Dispatch<IDispatchProps>) {
  }

  onClickD = (rowSelected: any) => this.dispatch({type: DeptActionTypes.CLICK, payload: rowSelected,});

  onDoubleClickD = (rowSelected: any) => this.dispatch({type: DeptActionTypes.DOUBLECLICK, payload: rowSelected,});

  onLoad = () => {
    fetch('http://www.mocky.io/v2/5b66a8693200006c00ee11bd')
      .then(response => {
        if (response.status === 200) {
          return response.json();
        } else {
          throw 'error';
        }
      })
      .then(data => {
        //формат ответа:
        console.log('fetch: data=', data);
        this.dispatch({type: `${DeptActionTypes.LOAD}`, payload: data.departments});
      })
      .catch(error => {
        this.dispatch({type: `${DeptActionTypes.LOAD}`, payload: error});
      });
};
onDelete = (departments: any) => {
  //const dept = departments.filter(department => (!(rowKeys.includes(department.id))));
    this.dispatch({type: DeptActionTypes.DELETE, payload: departments});
  };

  onInsert = (rowKey: any, departments: any) => {
    const dept = departments.concat(rowKey);
    this.dispatch({type: DeptActionTypes.INSERT, payload: dept});
  };

  onUpdate = (rowKey: any, departments: any, cellName: any, cellValue: any) => {
    const dept = departments.map(department => {
      if (department.id === rowKey) {
        department[cellName] = cellValue;
      }
      return department;
    });
    this.dispatch({type: DeptActionTypes.UPDATE, payload: dept});
  };
}
