import 'bootstrap/dist/css/bootstrap.min.css';
import * as UUID from 'uuid/v4';
import * as React from 'react';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import '../../node_modules/react-bootstrap-table/css/react-bootstrap-table.css';
import { IStoreState } from '../reducers';
import { Actions, IDispatchProps } from '../Actions/DeptActions';
import Modal from '../modal/modal';

// const departments = [
// {'id': '1', 'id_org': '1', 'name': 'Department_11', 'telephone': '11111'},
// {'id': '2', 'id_org': '1', 'name': 'Department_12', 'telephone': '22222'},
// {'id': '3', 'id_org': '2', 'name': 'Department_23', 'telephone': '33333'},
// {'id': '4', 'id_org': '2', 'name': 'Department_24', 'telephone': '44444'},
// {'id': '5', 'id_org': '3', 'name': 'Department_35', 'telephone': '55555'},
// {'id': '6', 'id_org': '3', 'name': 'Department_36', 'telephone': '66666'},
// {'id': '7', 'id_org': '4', 'name': 'Department_47', 'telephone': '77777'},
// {'id': '8', 'id_org': '4', 'name': 'Department_48', 'telephone': '88888'},
// {'id': '9', 'id_org': '5', 'name': 'Department_59', 'telephone': '99999'},
// {'id': '10', 'id_org': '5','name': 'Department_510', 'telephone': '10101'}
// ];

interface IStateProps {
  selectedId: any;
  data: any;
  id_org: any;
  name_org: string;
  isOpen: boolean;
}
interface IState {
  selectedId: any;
  data: any
  isOpen: boolean;
}
interface IStatePr {
  history: any;
}
type TProps = IDispatchProps & IStateProps & IStatePr;

class Department extends React.Component<TProps, IState> {
  constructor(props: any) {
    super(props);
    this.state = {selectedId: props.selectedId || '',
                  data: props.data || [],
                  isOpen: false};
  }

  onAfterSaveCell = (row, cellName, cellValue) => {
      const updatedRow = this.props.data.map(elem => {
        if (elem.id === row.id) {
          elem[cellName] = cellValue;
        }
        return elem;
      });
      this.props.actions.onUpdate(row, updatedRow, cellName, cellValue);
  }

  cellEditProp = {
      mode: 'dbclick',
      blurToSave: true,
      afterSaveCell: this.onAfterSaveCell
    };

    onRowSelectDept = (row) => {
    const {actions} = this.props;
    actions.onClickD(row.id);
  }

  selectRowProp = {
    mode: 'radio',
    bgColor: 'blue', // you should give a bgcolor, otherwise, you can't regonize which row has been selected
    hideSelectColumn: false,  // enable hide selection column.
    clickToSelect: true, // you should enable clickToSelect, otherwise, you can't select column.
    onSelect: this.onRowSelectDept
  };

  // onRowDoubleClick = (row) => {
  //   this.setState(prev => ({
  //     ...prev,
  //     ['selectedId']: row.id,
  //   }));
  // }
  customConfirm = (next, dropRowKeys) => {
    const dropRowKeysStr = dropRowKeys.join(',');
    if (confirm(`Вы действительно хотите удалить ${dropRowKeysStr}?`)) {
      next();
    }
  };

  onAfterDeleteRow = (rowKeys) => {
    const newData = this.props.data.filter(elem => (!(rowKeys.includes(elem.id))));
    this.props.actions.onDelete(newData);
  };

  onAfterInsertRow = (row) => {
    console.log('AFTERINSERT',this.props.id_org)
    const id = UUID();
    const id_org = this.props.id_org;
    row.id_org=id_org;
    const newRow = {id, ...row};
    console.log('2AFTERINSERT',id_org)
    this.props.actions.onInsert(newRow, this.props.data);
  };

  createCustomModalHeader = (onClose) => {
      return (
        <div className="modal-header" >
          <h3>Добавить строку</h3>
          <button className="btn btn-info" onClick={ onClose }>Close it!</button>
        </div>
      );
  }
  toggleModal = () => {
    this.setState(state => ({
      ...state,
      isOpen: !state.isOpen
    }));
    this.props.history.push(Modal);
  }
  createCustomButtonGroup = props => {
   return (
     <div className="my-custom-class">
       { props.insertBtn }
       { props.deleteBtn }
       <button type="button"
         className={ `btn btn-success` }
         onClick={this.toggleModal}>
         Редактировать
       </button>

     </div>
   );
 }

  options = {
    insertText: 'Добавить',
    deleteText: 'Удалить',
    handleConfirmDeleteRow: this.customConfirm,
    afterDeleteRow: this.onAfterDeleteRow,
    afterInsertRow: this.onAfterInsertRow,
    insertModalHeader: this.createCustomModalHeader,
    btnGroup: this.createCustomButtonGroup
  };

  componentWillMount() {
    if ( this.state.data.length === 0) {
      this.props.actions.onLoad();
    }
  }

  render() {
    console.log('DEPT.PROPS', this.props);
    console.log('DEPT.STATE', this.props.id_org);
    return (
      <div className="container">
        <div>
          <button
            id="move"
            type="button"
            className="btn btn btn-success"
            //onClick={this.onClickBtn}
            >Сотрудники (пока не работает)
          </button>
        </div>
        <div className="modal-dialog"></div>
        <div className="modal-content">
          <div className="header">
            <h4 className="text-center" id="myModalLabel ">Подразделения организации { this.props.name_org }
            </h4>
          </div>
          <div className="body">
            <div>
              <BootstrapTable data={ this.props.data} options = { this.options }
                selectRow={ this.selectRowProp } deleteRow insertRow cellEdit={ this.cellEditProp }
                striped hover condensed scrollTop={ 'Bottom' }>
              <TableHeaderColumn dataField="id" isKey hidden autoValue={ true } dataSort={ true }>ИД</TableHeaderColumn>
              <TableHeaderColumn dataField="id_org" hidden hiddenOnInsert>ИД_Орг</TableHeaderColumn>
              <TableHeaderColumn dataField="name" dataSort={ true }>Подразделение</TableHeaderColumn>
              <TableHeaderColumn dataField="telephone">Телефон</TableHeaderColumn>
            </BootstrapTable>
          </div>
        </div>
      </div>
    </div>
    );
  }
}

function mapStateToProps(state: IStoreState, ownProps): IStateProps {
  return {
  selectedId: state.departments.selectedId,
  data: state.departments.data.filter(department => department.id_org === ownProps.match.params.id_org),
  id_org: ownProps.match.params.id_org,
  name_org: state.organizations.data.filter(organization => organization.id === ownProps.match.params.id_org)[0].name,
  isOpen: state.departments.isOpen,
};
}

function mapDispatchToProps(dispatch: Dispatch<IDispatchProps>): IDispatchProps {
  return {
    actions: new Actions(dispatch)
  };
}
const connectDept = connect(mapStateToProps, mapDispatchToProps)(Department);

export {connectDept as Department};
