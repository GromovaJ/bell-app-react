import 'bootstrap/dist/css/bootstrap.min.css';
import * as UUID from 'uuid/v4';
import * as React from 'react';
import * as ReactModal from 'react-modal';
import { BootstrapTable, TableHeaderColumn } from 'react-bootstrap-table';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import '../../node_modules/react-bootstrap-table/css/react-bootstrap-table.css';
import { IStoreState } from '../reducers';
import { Actions, IDispatchProps } from '../Actions/OrgActions';
import Modal from '../modal/modal';

//const newId = require('uuid/v4');
// const organizations = [
// {"id": "1", "name": "Organization_1", "address": "Address 1, 1", "inn": "INN_1"},
// {"id": "2", "name": "Organization_2", "address": "Address 2, 2", "inn": "INN_2"},
// {"id": "3", "name": "Organization_3", "address": "Address 3, 3", "inn": "INN_3"},
// {"id": "4", "name": "Organization_4", "address": "Address 4, 4", "inn": "INN_4"},
// {"id": "5", "name": "Organization_5", "address": "Address 5, 5", "inn": "INN_5"},
// {"id": "6", "name": "Organization_6", "address": "Address 6, 6", "inn": "INN_6"},
// {"id": "7", "name": "Organization_7", "address": "Address 7, 7", "inn": "INN_7"},
// {"id": "8", "name": "Organization_8", "address": "Address 8, 8", "inn": "INN_8"},
// {"id": "9", "name": "Organization_9", "address": "Address 9, 9", "inn": "INN_9"},
// {"id": "10", "name": "Organization_10", "address": "Address 10, 10 ", "inn": "INN_10"}
// ];
//const tableHeader= ['id', 'name', 'address', 'inn'];

interface IStateProps {
  selectedOrg: any;
  data: any;
  isOpen: boolean;
}
interface IState {
  selectedOrg: any;
  data: any;
  isOpen: boolean;
}
interface IStatePr {
  history: any;
}
type TProps = IDispatchProps & IStateProps & IStatePr;

class Organization extends React.Component<TProps, IState> {
  constructor(props: any) {
    super(props);
    this.state = {selectedOrg: props.selectedOrg || '',
                  data: props.data || [],
                  isOpen: false
                  }
                 };

createCustomModalHeader = (onClose) => {
    return (
      <div className="modal-header" >
        <h3>Добавить строку</h3>
        <button className="btn btn-info" onClick={ onClose }>Close it!</button>
      </div>
    );
}
  componentWillMount() {
    if ( this.state.data.length === 0) {
      this.props.actions.onLoad();
    }
  }

  // componentWillReceiveProps(nextProps: any) {
  //   this.setState(prev => ({
  //     ...prev,
  //     ['data']: nextProps.data,
  //   }));
  // }

  onAfterSaveCell = (row, cellName, cellValue) => {
      const updatedRow = this.props.data.map(elem => {
        if (elem.id === row.id) {
          elem[cellName] = cellValue;
        }
        return elem;
      });
      this.props.actions.onUpdate(row, updatedRow, cellName, cellValue);
  }

  cellEditProp = {
      mode: 'dbclick',
      blurToSave: true,
      afterSaveCell: this.onAfterSaveCell
    };

  onClickBtn = () => {
    const dept = `/dept/${this.props.selectedOrg}`;
      this.props.history.push(dept);

  };

  handleClick = (row) => {
    this.props.actions.onClick(row.id);
  }

  selectRowProp = {
    mode: 'radio',
    bgColor: 'blue', // you should give a bgcolor, otherwise, you can't regonize which row has been selected
    hideSelectColumn: false,  // enable hide selection column.
    clickToSelect: true, // you should enable clickToSelect, otherwise, you can't select column.
    onSelect: this.handleClick

  };

  onAfterInsertRow = (row) => {
    const id = UUID();
    const newRow = {id, ...row};
    this.props.actions.onInsert(newRow, this.props.data);
  };

  customConfirm = (next, dropRowKeys) => {
    const dropRowKeysStr = dropRowKeys.join(',');
    if (confirm(`Вы действительно хотите удалить ${dropRowKeysStr}?`)) {
      next();
    }
  };

  onAfterDeleteRow = (rowKeys) => {
    const newData = this.props.data.filter(elem => (!(rowKeys.includes(elem.id))));
    this.props.actions.onDelete(newData);
  };

  toggleModal = () => {
    // console.log('toggleModal-state', this.state);
    // console.log('1toggleModal-state', !this.state.isOpen);

    //setState - убрать в reducer
    this.setState(state => ({
      ...state,
      isOpen: !state.isOpen
    }));
    this.props.history.push(Modal);
  }

  createCustomButtonGroup = props => {
   return (
     <div className="my-custom-class">
       { props.insertBtn }
       { props.deleteBtn }
       <button type="button"
         className={ `btn btn-success` }
         onClick={this.toggleModal}>
         Редактировать
       </button>
     </div>
   );
 }
 options = {
   insertText: 'Добавить',
   deleteText: 'Удалить',
   handleConfirmDeleteRow: this.customConfirm,
   afterDeleteRow: this.onAfterDeleteRow,
   afterInsertRow: this.onAfterInsertRow,
   insertModalHeader: this.createCustomModalHeader,
   btnGroup: this.createCustomButtonGroup
 };

  render() {
    console.log('ORG.PROPS', this.props);
    console.log('ORG.STATE', this.state);

    return (
    <div className="container">
      <div>
        <button
          id="move"
          type="button"
          className="btn btn btn-primary"
          onClick={this.onClickBtn}
          >Подразделения
        </button>
        </div>
        <ReactModal
          isOpen={this.state.isOpen}
          contentLabel="Minimal Modal Example"
       >
         <Modal
           //show={this.state.isOpen}
           onClose={this.toggleModal}>

         </Modal>
       </ReactModal>
       <div >
       <div className="content">

         <div className="header">
           <h4 className="text-center" id="myModalLabel ">Организации</h4>
         </div>
         <div className="body">
           <BootstrapTable data={ this.props.data} options = { this.options }
             selectRow={ this.selectRowProp } insertRow deleteRow cellEdit={ this.cellEditProp }
             striped hover condensed scrollTop={ 'Bottom' }>
             <TableHeaderColumn dataField="id" isKey hidden autoValue={ true }>ИД</TableHeaderColumn>
             <TableHeaderColumn dataField="name" dataSort={ true }>Организация</TableHeaderColumn>
             <TableHeaderColumn dataField="address" dataSort={ true }>Адрес</TableHeaderColumn>
             <TableHeaderColumn dataField="inn" dataSort={ true }>ИНН</TableHeaderColumn>
           </BootstrapTable>

         </div>
       </div>
       </div>

      </div>
    );
  }
}

function mapStateToProps(state: IStoreState): IStateProps {
console.log('mapStateToProps-state', state);
  return {
  selectedOrg: state.organizations.selectedOrg,
  data: state.organizations.data,
  isOpen: state.organizations.isOpen
  };
}
function mapDispatchToProps(dispatch: Dispatch<IDispatchProps>): IDispatchProps {
  return {
    actions: new Actions(dispatch)
  };
}
const connectOrg = connect(mapStateToProps, mapDispatchToProps)(Organization);

export {connectOrg as Organization};
