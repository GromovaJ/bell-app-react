//import { routerReducer } from 'react-router-redux';
import {Action, combineReducers} from 'redux';
import {departments} from './departments';
import {login} from './login';
import {organizations} from './organizations';

export interface IActionType extends Action {
  type: string;
  payload: any;
}

export interface IStoreState {
  user: string,
  password: string,
  loginStatus: boolean,
  loading: boolean,
  organizations: any,
  departments: any,
  login: any,
  selectedId: string,
  selectedOrg: string,
  isOpen: boolean,
//  data: any,
}

export const reducer = combineReducers({
  login,
  organizations,
  departments,
  //routing: routerReducer
});
