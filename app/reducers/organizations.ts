import {Action} from 'redux';
import {OrgActionTypes} from '../Actions/Consts';

export interface IActionType extends Action {
  type: string;
  payload: any;
}
export interface IStoreState {
  selectedOrg: string,
  data: any,
}
const initialState = {
      selectedOrg: '',
      data: [],
}

export function organizations (state: any = initialState, action: IActionType) {
  switch (action.type) {
    case OrgActionTypes.ORGCLICK:
    return {
      ...state,
      selectedOrg: action.payload,
    };

    case OrgActionTypes.ORGDOUBLECLICK:
    return {
      ...state,
      selectedOrg: action.payload,
    };
    case OrgActionTypes.ORGLOAD:
    return {
      ...state,
      data: action.payload,
      selectedOrg: '',
      isOpen: false,
    };
    case OrgActionTypes.ORGDELETE:
    return {
      ...state,
      data: action.payload,
      selectedOrg: '',
    };
    case OrgActionTypes.ORGINSERT:
    return {
      ...state,
      data: action.payload,
    };
    case OrgActionTypes.ORGUPDATE:
    return {
      ...state,
      data: action.payload,
    };
  }
  return state;
}
