import {Action} from 'redux';
import {DeptActionTypes} from '../Actions/Consts';

export interface IActionType extends Action {
  type: string;
  payload: any;
}
export interface IStoreState {
  selectedId: string,
  data: any
}
const initialState = {
      selectedId: '',
      data: [],
}

export function departments (state: any = initialState, action: IActionType) {
  switch (action.type) {
    case DeptActionTypes.CLICK:
    return {
      ...state,
      selectedId: action.payload,
    };

    case DeptActionTypes.DOUBLECLICK:
    return {
      ...state,
      selectedId: action.payload,
    };
    case DeptActionTypes.LOAD:
    return {
      ...state,
      data: action.payload,
      selectedId: '',
    };
    case DeptActionTypes.DELETE:
    return {
      ...state,
      data: action.payload,
      selectedId: '',
    };
    case DeptActionTypes.INSERT:
    return {
      ...state,
      data: action.payload,
    };
    case DeptActionTypes.UPDATE:
    return {
      ...state,
      data: action.payload,
    };
  }
  return state;
}
