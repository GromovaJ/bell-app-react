import {Action} from 'redux';
import {isCorrectLogin} from '../common/isCorrectLogin';
import {ActionTypes, AsyncActionTypes} from '../Actions/Consts';

export interface IActionType extends Action {
  type: string;
  payload: any;
}
export interface IStoreState {
  user: string,
  password: string,
  loginStatus: boolean,
  loading: boolean
}
const initialState = {
      user: '',
      password: '',
      loginStatus: false,
      loading: false
}

export function login (state: IStoreState = initialState, action: IActionType) {
  switch (action.type) {

    case `${ActionTypes.LOGIN}${AsyncActionTypes.BEGIN}`:
    return {
      ...state,
      loading: true,
    };
    let loginStatus;
    case `${ActionTypes.LOGIN}${AsyncActionTypes.SUCCESS}`:
    if (isCorrectLogin(action.payload.user, action.payload.password)){
      loginStatus = action.payload.data.authorized;
    } else {
      loginStatus = false;
    }
    return {
      ...state,
      loginStatus,
      loading: false,
      user: action.payload.user,
      password: action.payload.password,
    };

    case `${ActionTypes.LOGIN_USER}`:
    return {
      ...state,
      user: action.payload,
    };

    case `${ActionTypes.LOGIN_PASSWORD}`:
    return {
      ...state,
      password: action.payload,
    };

    case `${ActionTypes.LOGIN}${AsyncActionTypes.FAILURE}`:
    return {
      ...state,
      loading: false,
      loginStatus: false,
      user: action.payload.user,
      password: action.payload.password,
    };

    case ActionTypes.LOGOUT:
    return {
      ...state,
      loginStatus: false,
      user: action.payload.user,
      password: action.payload.password,
    };
  }
  return state;
}
